package fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.fragment_task.R

class HomeFragment: Fragment(R.layout.home_fragment) {
    private lateinit var editText: EditText
    private lateinit var post_btn:Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editText = view.findViewById(R.id.editText)
        post_btn = view.findViewById(R.id.post_btn)

        val navController = Navigation.findNavController(view)

        post_btn.setOnClickListener {
            val amount = editText.text.toString()
            if (amount.isEmpty()){
                return@setOnClickListener
            }

            val finalAmount = amount.toString().toInt()

            val action = HomeFragmentDirections.actionHomeFragmentToDashboardFragment(finalAmount)
            navController.navigate(action)
        }
    }
}